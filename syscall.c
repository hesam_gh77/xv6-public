#include "types.h"
#include "defs.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "x86.h"
#include "syscall.h"
#include "test.h"
#include "spinlock.h"


//Table myTable;
// User code makes a system call with INT T_SYSCALL.
// System call number in %eax.
// Arguments on the stack, from the user call to the C
// library system call function. The saved user %esp points
// to a saved program counter, and then the first argument.
// Fetch the int at addr from the current process.
//struct Table mainTable;
//struct Table* mainTable;
struct Table mainTable;
struct spinlock mylock;
//struct Table mainTable;

int
fetchint(uint addr, int *ip)
{

  struct proc *curproc = myproc();

  if(addr >= curproc->sz || addr+4 > curproc->sz)
    return -1;
  *ip = *(int*)(addr);
  return 0;
}

// Fetch the nul-terminated string at addr from the current process.
// Doesn't actually copy the string - just sets *pp to point at it.
// Returns length of string, not including nul.
int
fetchstr(uint addr, char **pp)
{
  char *s, *ep;
  struct proc *curproc = myproc();

  if(addr >= curproc->sz)
    return -1;
  *pp = (char*)addr;
  ep = (char*)curproc->sz;
  for(s = *pp; s < ep; s++){
    if(*s == 0)
      return s - *pp;
  }
  return -1;
}

// Fetch the nth 32-bit system call argument.
int
argint(int n, int *ip)
{
  return fetchint((myproc()->tf->esp) + 4 + 4*n, ip);
}

// Fetch the nth word-sized system call argument as a pointer
// to a block of memory of size bytes.  Check that the pointer
// lies within the process address space.
int
argptr(int n, char **pp, int size)
{
  int i;
  struct proc *curproc = myproc();
 
  if(argint(n, &i) < 0)
    return -1;
  if(size < 0 || (uint)i >= curproc->sz || (uint)i+size > curproc->sz)
    return -1;
  *pp = (char*)i;
  return 0;
}

// Fetch the nth word-sized system call argument as a string pointer.
// Check that the pointer is valid and the string is nul-terminated.
// (There is no shared writable memory, so the string can't change
// between this check and being used by the kernel.)
int
argstr(int n, char **pp)
{
  int addr;
  if(argint(n, &addr) < 0)
    return -1;
  return fetchstr(addr, pp);
}

extern int sys_chdir(void); //1
extern int sys_close(void);//2
extern int sys_dup(void);//3
extern int sys_exec(void);//4
extern int sys_exit(void);//5
extern int sys_fork(void);//6
extern int sys_fstat(void);//7
extern int sys_getpid(void);//8
extern int sys_kill(void);//9
extern int sys_link(void);//10
extern int sys_mkdir(void);//11
extern int sys_mknod(void);//12
extern int sys_open(void);//13
extern int sys_pipe(void);//14
extern int sys_read(void);//15
extern int sys_sbrk(void);//16
extern int sys_sleep(void);//17
extern int sys_unlink(void);//18
extern int sys_wait(void);//19
extern int sys_write(void);//20
extern int sys_uptime(void);//21
extern int sys_count(void);//22

static int (*syscalls[])(void) = {
[SYS_fork]    sys_fork,//1
[SYS_exit]    sys_exit,//2
[SYS_wait]    sys_wait,//3
[SYS_pipe]    sys_pipe,//4
[SYS_read]    sys_read,//5
[SYS_kill]    sys_kill,//6
[SYS_exec]    sys_exec,//7
[SYS_fstat]   sys_fstat,//8
[SYS_chdir]   sys_chdir,//9
[SYS_dup]     sys_dup,//10
[SYS_getpid]  sys_getpid,//11
[SYS_sbrk]    sys_sbrk,//12
[SYS_sleep]   sys_sleep,//13
[SYS_uptime]  sys_uptime,//14
[SYS_open]    sys_open,//15
[SYS_write]   sys_write,//16
[SYS_mknod]   sys_mknod,//17
[SYS_unlink]  sys_unlink,//18
[SYS_link]    sys_link,//19
[SYS_mkdir]   sys_mkdir,//20
[SYS_close]   sys_close,//21
[SYS_count]   sys_count,//22
};

void
syscall(void)
{ acquire(&mylock);

  int num;
  struct proc *curproc = myproc();

  num = curproc->tf->eax;
  //cprintf("%d\n",num);
  mainTable.array[num-1]=mainTable.array[num-1]+1;
  release(&mylock);
  if(num > 0 && num < NELEM(syscalls) && syscalls[num]) {
      curproc->tf->eax = syscalls[num]();

  } else {
    cprintf("%d %s: unknown sys call %d\n",
            curproc->pid, curproc->name, num);
    curproc->tf->eax = -1;
  }
}
